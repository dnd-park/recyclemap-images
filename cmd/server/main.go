package main

import (
	"database/sql"
	"fmt"
	"gitlab.com/dnd-park/recyclemap-images/internal"
	"gitlab.com/dnd-park/recyclemap-images/store"
	"log"
	"net/http"
	"os"
)

func main() {
	err := Start()
	if err != nil {
		println(err)
	}
}

func Start() error {
	config := internal.NewConfig()

	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = ":9000"
	} else {
		port = ":" + port
	}
	config.BindAddr = port

	url := os.Getenv("DATABASE_URL")
	if len(url) != 0 {
		config.DatabaseURL = url
	}

	srv, err := internal.NewServer(config)
	if err != nil {
		return err
	}

	db, err := newDB(config.DatabaseURL)
	if err != nil {
		log.Println(err)
		return err
	}

	defer func() {
		if err := db.Close(); err != nil {
			log.Println(err)
		}
	}()

	srv.ConfigureServer(db)

	fmt.Println("Start server on port " + config.BindAddr)

	return http.ListenAndServe(config.BindAddr, srv)
}

func newDB(dbURL string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dbURL)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(100)
	db.SetMaxIdleConns(100)
	if err := store.CreateTables(db); err != nil {
		return nil, err
	}
	return db, nil
}
