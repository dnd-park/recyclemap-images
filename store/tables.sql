CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS images (
      id uuid DEFAULT uuid_generate_v4 (),
      image bytea,
      primary key (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idxImageID ON images (id);