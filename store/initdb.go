package store

import (
	"database/sql"
	_ "github.com/lib/pq"
	"io/ioutil"
)

func CreateTables(db *sql.DB) error {
	file, err := ioutil.ReadFile("store/tables.sql")
	if err != nil {
		return err
	}

	_, err = db.Exec(string(file))
	if err != nil {
		return err
	}

	return nil
}
