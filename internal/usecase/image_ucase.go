package usecase

type repository interface {
	Create(image [][]byte) ([]string, error)
	Get(id string) ([]byte, error)
}

type ImageUsecase struct {
	repository repository
}

func NewUsecase(repository repository) *ImageUsecase {
	return &ImageUsecase{repository: repository}
}


func (u *ImageUsecase) Create(datas [][]byte) ([]string, error) {
	return u.repository.Create(datas)
}

func (u *ImageUsecase) Get(id string) ([]byte, error) {
	return u.repository.Get(id)
}