package internal

type Config struct {
	BindAddr    string
	LogLevel    string
	DatabaseURL string
}

func NewConfig() *Config {
	return &Config{
		BindAddr:    ":9000",
		LogLevel:    "debug",
		DatabaseURL: "host=localhost dbname=recyclemap sslmode=disable port=5432 password=1234 user=d",
	}
}