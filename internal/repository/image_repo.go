package repository

import (
	"database/sql"
	"strconv"
	"strings"
)

type ImageRepository struct {
	db *sql.DB
}

func NewImageRepository(db *sql.DB) *ImageRepository {
	return &ImageRepository{db}
}

func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)
	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}
	return old
}

func (r *ImageRepository) Create(images [][]byte) ([]string, error) {
	sqlStr := "INSERT INTO images (image) VALUES "
	var values []interface{}

	for _, image := range images {
		sqlStr += "(?),"
		values = append(values, image)
	}

	sqlStr = strings.TrimSuffix(sqlStr, ",")

	sqlStr = ReplaceSQL(sqlStr, "?")

	sqlStr += " RETURNING id"

	stmt, err := r.db.Prepare(sqlStr)
	if err != nil {
		return nil, err
	}

	defer stmt.Close()

	rows, err := stmt.Query(values...)
	if err != nil {
		return nil, err
	}

	ids := make([]string, 0, len(images))
	for rows.Next() {
		var id string
		err := rows.Scan(&id)
		if err != nil {
			return nil, err
		}
		ids = append(ids, id)
	}

	return ids, nil
}

func (r *ImageRepository) Get(id string) ([]byte, error){
	image := make([]byte, 0)

	if err := r.db.QueryRow(
		`SELECT image FROM images WHERE id::text = $1`,
		id,
	).Scan(
		&image,
	); err != nil {
		return nil, err
	}
	return image, nil
}
