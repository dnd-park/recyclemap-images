package handlers

import (
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/dnd-park/recyclemap-images/internal/general"
	"io/ioutil"
	"net/http"
)

type usecase interface {
	Create(images [][]byte) ([]string, error)
	Get(id string) ([]byte, error)
}

type ImageHandler struct {
	usecase			usecase
}

func NewImageHandler(m *mux.Router, ucase usecase) {
	handler := &ImageHandler{
		usecase:   	  ucase,
	}

	m.HandleFunc("/", handler.CreateImages).Methods(http.MethodPost)
	m.HandleFunc("/{id}", handler.GetImage).Methods(http.MethodGet)
}

func (h *ImageHandler) CreateImages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if err := r.ParseMultipartForm(1000000); err != nil {
		general.Error(w,r, http.StatusBadRequest, errors.New("wrong input"))
		return
	}
	m := r.MultipartForm

	files := m.File["images"]

	images := make([][]byte, 0, len(files))
	for i, _ := range files {
		file, err := files[i].Open()
		if err != nil {
			general.Error(w, r, http.StatusInternalServerError, errors.New("something goes wrong"))
			return
		}

		image, err := ioutil.ReadAll(file)
		if err != nil {
			general.Error(w, r, http.StatusInternalServerError, errors.New("something goes wrong"))
			return
		}

		if err = file.Close(); err != nil {
			general.Error(w, r, http.StatusInternalServerError, errors.New("something goes wrong"))
			return
		}

		images = append(images, image)
	}

	id, err := h.usecase.Create(images)
	if err != nil {
		general.Error(w, r, http.StatusInternalServerError, errors.New(err.Error()))
		return
	}

	general.Respond(w, r, http.StatusCreated, map[string][]string{"ids": id})
}

func (h *ImageHandler) GetImage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/jpg")
	vars := mux.Vars(r)

	image, err := h.usecase.Get(vars["id"])
	if err != nil {
		general.Error(w,r, http.StatusInternalServerError, err)
		return
	}
	_, _ = w.Write(image)
}
