package internal

import (
	"database/sql"
	"github.com/gorilla/mux"
	"gitlab.com/dnd-park/recyclemap-images/internal/handlers"
	"gitlab.com/dnd-park/recyclemap-images/internal/repository"
	"gitlab.com/dnd-park/recyclemap-images/internal/usecase"
	"net/http"
)

type Server struct {
	Mux    *mux.Router
	Config *Config
}

func NewServer(config *Config) (*Server, error) {
	s := &Server{
		Mux:    mux.NewRouter(),
		Config: config,
	}
	return s, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Mux.ServeHTTP(w, r)
}

func (s *Server) ConfigureServer(db *sql.DB) {

	repo := repository.NewImageRepository(db)

	ucase := usecase.NewUsecase(repo)

	handlers.NewImageHandler(s.Mux, ucase)
}

